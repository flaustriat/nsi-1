# Découverte de Python 3 

Les liens "Binder" pointent vers des fichiers interactifs jupyter notebook.
Pour executer le contenu d'une cellule  d'un notebook taper `shift`+`entrée`

* Partie 1: Les nombres et les variables
	* [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F1_nombres%2FdecouvertePython.ipynb)
	* [Cours non interactif](https://framagit.org/flaustriat/nsi-1/blob/master/1_decouverte_python/1_nombres/decouvertePython.ipynb)
	* [Fiche TD](https://framagit.org/flaustriat/nsi-1/blob/master/1_decouverte_python/1_nombres/TD_nombresVariables.pdf)
	* [Fiche TP](https://framagit.org/flaustriat/nsi-1/blob/master/1_decouverte_python/1_nombres/TP_nombres.pdf)
* Partie 2: Premier programme
	* [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F2_premiers_programmes%2Fpremier_programme.ipynb)
	* [Premier programme correction](https://framagit.org/flaustriat/nsi-1/blob/master/1_decouverte_python/2_premiers_programmes/premier_programme_corr.ipynb)
	* [Fiche TP](https://framagit.org/flaustriat/nsi-1/blob/master/1_decouverte_python/2_premiers_programmes/TP_premiers_programmes.pdf)
* Partie 3: Notion de fonction
	* [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F3_notion_fonction%2Fdecouverte_python_fonctions.ipynb)
	* [Cours non interactif](https://framagit.org/flaustriat/nsi-1/blob/master/1_decouverte_python/3_notion_fonction/decouverte_python_fonctions.ipynb)
	* [Fiche TP](https://framagit.org/flaustriat/nsi-1/blob/master/1_decouverte_python/3_notion_fonction/TP_fonctions.pdf)
* Partie 4: Chaînes de caractères et booléens
	* [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F4_booleens_string%2FdecouvertePython_bool_string.ipynb)
	* [Cours non interactif](https://framagit.org/flaustriat/nsi-1/blob/master/1_decouverte_python/4_booleens_string/decouvertePython_bool_string.ipynb)
	* [Fiche TP](https://framagit.org/flaustriat/nsi-1/blob/master/1_decouverte_python/4_booleens_string/TD_booleens.pdf)
* Partie 5: Conditions et branchements 
	*  [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F5_conditions%2FdecouvertePythonConditions.ipynb)
	* [cours non interactif](https://framagit.org/flaustriat/nsi-1/blob/master/1_decouverte_python/5_conditions/decouvertePythonConditions.ipynb)
	* [Fiche TP](https://framagit.org/flaustriat/nsi-1/blob/master/1_decouverte_python/5_conditions/TP_tests_conditions.pdf)
* Partie 6: Boucles "Tant que"
	* [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F6_boucles_tant_que%2Fdecouverte_python_boucle_tant_que.ipynb))
	
	* [Cours non interactif](6_boucles_tant_que/decouverte_python_boucle_tant_que.ipynb)
	
	* [Fiche TP (pdf)](6_boucles_tant_que/TP_tant_que.pdf)
	
	* [Fiche TP (lecture en ligne)](6_boucles_tant_que/TP_tant_que.md)
* Partie 7: Listes
	* [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F7_listes%2Fliste_python1.ipynb)
	  
	* [Cours non interactif](7_listes/liste_python1.ipynb)

* Partie 8: Boucles "Pour"
	* [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F8_boucles_for%2Fboucle_for.ipynb)
	* [Cours non interactif](8_boucles_for/boucle_for.ipynb)
* Partie 9: Tuple dictionnaire
	* [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F9_tuple_dict%2Ftuple-dict.ipynb)
	* [Cours non interactif](9_tuple_dict/tuple-dict.ipynb)
* Partie 10: Listes par compréhension -listes de listes / matrices
	* [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fflaustriat%2Fnsi-1/master?filepath=%2F1_decouverte_python%2F10_listes_comprehension%2Fliste2_liste_comprehension.ipynb)
	* [Cours non interactif](10_listes_comprehension/liste2_liste_comprehension.ipynb)
	  
	

	
