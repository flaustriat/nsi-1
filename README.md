# NSI-1

## Documents pour le cours de spécialité NSI première

Une page de ressources: <https://framagit.org/flaustriat/nsi-1/blob/master/sources.md>

## Python en ligne: 

Bac à Sable pour Python <https://python.infobrisson.fr/>. Un site pour tester des programmes Python sans l'installer sur une machine.



## Documents de cours

*  [Découverte de Python 3 :](1_decouverte_python/decouverte_python.md)
*  [Systèmes d'exploitation](3_systemes_exploitation)
*  [Données en tables](2_donnees_tables)
*  [Algorithme 1](4_algorithme_1)
*  [Web](5_web/README.md)





## Travail à faire

* Pour le mardi 24 mars:
   Données en tables
  Imprimer le TP sur la fusion de tables compléter la page 2 puis faire la partie programmation en Python soit à l'aide des fichiers joints soit à l'aide du notebook.
  Imprimer, si possible, le document sur la recherche séquentielle et sur la recherche dichotomique contenu dans le répertoire [Algorithme 1](4_algorithme_1) .
  
* Pour le vendredi 27 mars:
  Faire le [notebook](http://py.ac-paris.fr/6b4406b6) sur la recherche dichotomique.

* Vendredi 27 mars:
   Correction noteBook sur la recherche dichotomique

* Pour mardi 31 mars :
  
  Exemple de [sujet E3C en NSI](docs/Q01-SUJET.pdf) : faire l'ensemble du sujet sauf partie D et les questions G.1 et G.3.
  
  Me l'envoyer par mail vos réponses.
  
* Pour le vendredi 3 avril:  Faire le [noteBook](http://py.ac-paris.fr/b6f559dd) sur le tri par insertion.

* Pour le 20 avril:  [sujet E3C en NSI](docs/Q02-SUJET.pdf) sauf partie D et questions E.5 et E.6 .
   Prenez le temps de relire vos cours avant de répondre, de poser les calculs ou de faire la trace du programme avant de répondre. Me renvoyer par mail vos réponses.
   Le plus simple pour moi est que vous me les envoyez sous la forme :
    Thème A: ABCDXA , etc. C'est-à-dire le thème suivant de vos réponses dans l'ordre avec X si pas de réponse.
   
*  Cours du 20 avril:
  
   *  correction des parties A,B et C du [QCM](docs/Q02-SUJET.pdf) .
   * [Cours sur les objets connectés](http://timtic.fr/snt-7-informatique-embarquee-et-objets-connectes/)
   * [Exercices objets connectés sur france IOI](https://amazon.quick-pi.org/) exercice 1 :star:  :star:et :star:  :star: :star: et exercice 4 :star:  :star: .
   
* Pour le vendredi 24 avril: faire les [exercices objets connectés sur france IOI](https://amazon.quick-pi.org/) de la série commencée en classe virtuelle.

* Cours du 24 avril: Correction QCM, et exercices sur les objets connectés sur france IOI. 
   Début du cours sur le HTML.

* Pour le mardi 28 avril: À faire vous même 3 du cours TP sur HTML/css (installer NotePad ++ et l'avoir sur l'ordinateur utilisé pour le cours).

* Cours du mardi 28 avril: Cours/TP sur HTML et CSS. 

* Pour le mardi 5 mai faire le mini projet décrit à la fin du cours à partir des fichiers donnés.
   L'ensemble des documents est [la](5_web/README.md)

* Liens vers des ressources sur les objets connectés:
  
   * [Exercices objets connectés sur france IOI](https://amazon.quick-pi.org/)
   * [Cours sur les objets connectés](http://timtic.fr/snt-7-informatique-embarquee-et-objets-connectes/)
   
   










