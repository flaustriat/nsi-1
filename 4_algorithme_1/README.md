# Algorithme 1

## Recherche d'un élément dans une liste

### Recherche séquentielle

* [TD](algo1-rechercheSéquentielle.pdf)
* [noteBook](http://py.ac-paris.fr/0fe96009)
* Correction du [noteBook](rechercheSequentielle.pdf)

### Recherche dichotomique dans une liste triée

* [TD](algo2-rechercheDicho.pdf)
* [noteBook](http://py.ac-paris.fr/6b4406b6)
* Correction du [noteBook](rechercheDicho.pdf)

### Tri d'une liste
* [Cours tri par insertion - tri par selection](algo1-tris.pdf)
* [Visualisation de différents algorithme de tri](http://inriamecsci.github.io/#!/grains/methodes-tri)
* [Article sur les tris](https://interstices.info/les-algorithmes-de-tri/)
* [noteBook tri par insertion](http://py.ac-paris.fr/b6f559dd)
* [Cours sur la complexité](algo1-complexité.pdf)
* [noteBook comparaison de tris](http://py.ac-paris.fr/58f67057) 









