# -*- coding: utf-8 -*-
"""
Created on Mon Oct  7 09:15:14 2019

@author: François Laustriat

Module de fonctions pour le projet présentation
"""

import datetime as dt
import re


## Calcul de l'âge à partir de la date de naissance:
### https://stackoverflow.com/questions/2217488/age-from-birthdate-in-python
def calculate_age(born):
    """
    Date ->Int
    Retourne un entier représentant le nombre d'année entre la date contenue dans 
    l'objet Date et la date courante.
    """
    today = dt.date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

def calcul_age(naiss):
    """
    str -> int
    Prend en argument une chaine de caractères représentant une date au format
    "JJ/MM/YYYY"
    et renvoi la différence en année avec la date courrante.
    """
    jour, mois, annee = naiss.split('/')
    date_naiss = dt.date(int(annee),int(mois),int(jour))
    return calculate_age(date_naiss)


def est_date(chaine):
    """ 
    String -> Bool
    Renvoie Vrai si chaine est de la forme 'JJ/MM/AAAA'
    '00/00/0000' et '99/99/9999' renvoient Vrai.
    
    """
    if re.match('\d{2}/\d{2}/\d{4}',chaine):
        return True
    else:
        return False
 

def est_email(chaine):
    """
    String -> Bool
    Renvoie Vrai si chaine peut être une adresse email valide.
    Ici une adresse email valide est de la forme A@B.C où A est une chaine 
    alphanumérique pouvant contenir _ et . B et C sont des chaînes de caractères 
    alphanumériques
    """
    if re.match('[a-zA-Z0-9_.]+@\w+\.\w+',chaine):
        return True
    else:
        return False
