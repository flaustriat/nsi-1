1. Fichiers CSV
    * [Attente en gare](DonnéesStructuréesCVS.pdf)
    * [Données sur les gares](gares-pianos.csv)
2. Traitement du fichier provenant de l'INSEE sur les prénoms
    * [TP sur les prénoms](TraitementDonnées1Prénoms.pdf)
    * [Données INSE sur les prénoms](nat2018.csv)
3. Fusion de tables
	* [TP sur la fusion de tables](TraitementDonnées2fusions2.pdf)
	* [Fichiers à télécharger](fichiers)
	* Si vous n'avez pas accès à un ordinateur avec Python vous pouvez utiliser et compléter ce [notebook](https://py.ac-paris.fr/b65998b6 )
