# -*- coding: utf-8 -*-
"""

@title: Fusion de tables
"""

#Warning car on ne s'en sert que dans la fonction de fusion.
from copy import deepcopy
import csv

def charger_fichier(nom_fich):
    """ string -> List[dict]
    prédicat: le séparateur utilisé est ","
    Charge le fichier nom_fich et retourne une liste de dictionnaires contentant
    les valeurs du fichier
    """
    liste = []
    with open(nom_fich,"r", encoding ="utf -8") as csvfile :
        lecteur = csv.DictReader(csvfile , delimiter =",")
        for ligne in lecteur :
            liste.append(ligne)
    return liste


def affiche_table(table):
    """
    list[dict] -> None
    prédicat: la liste est non vide
    Affiche ligne à ligne la table table en colonnes de 12 caractères de large.
    """
    #Affiche l'entête
    format_tableau ="".join( [ "{"+str(i)+":<12}" for i in range(len(table[0]))])
    print(format_tableau.format(*list(table[0].keys())))
    ##Affiche le condenu ligne par ligne
    for ligne in table:
        print(format_tableau.format(*list(ligne.values())))
        

