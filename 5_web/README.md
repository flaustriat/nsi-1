# Le Web



## HTML et CSS

* [Cours -TP](html-1-html.pdf)
* [Fichier joint au TP](html_css.zip)
* [Lien vers l'éditeur de texte notePad ++](https://notepad-plus-plus.org/downloads/) télécharger la dernière  version en 64 bits

## Javascript 

* [Cours-TP](html-2-javascript.pdf)
* [Fichier joint au TP](javascript.zip)
* Le TP se fait en utilisant le navigateur [Firefox](https://www.mozilla.org/fr/firefox/new/) 

## Un serveur en Python- méthode GET et POST

*  [Cours-TP](html-3-client-serveur.pdf)
* [Fichier joint au TP](formulaire.zip)
* Le TP se fait en utilisant le navigateur [Firefox](https://www.mozilla.org/fr/firefox/new/) et l'éditeur [notePad ++](https://notepad-plus-plus.org/downloads/)






