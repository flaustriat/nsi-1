# Sitographie

## Sites de références

### NSI

* <https://pixees.fr/informatiquelycee/n_site/nsi_prem.html>
* <https://sites.google.com/site/thibautdeguillaume/home>

### Python

* <https://docs.python.org/3/index.html>
* <https://fr.wikibooks.org/wiki/Programmation_Python>
* Livre de Gérard Swinnen sur Python 3 <https://inforef.be/swi/download/apprendre_python3_5.pdf> (licence creative commmons)
* Documentation TkInter : <http://tkinter.fdex.eu/index.html>

### HTML CSS

* HTML:
	* Documentation <http://w3schools.com/tags/> 
	* Validateur en ligne : <https://validator.w3.org/#validate_by_upload>
* CSS: 
	* Documentation <http://www.w3schools.com/css/>
	* Validateur en ligne : <https://validator.w3.org/unicorn/?ucn_lang=fr#validate-by-upload+task_conformance>

## Sites institutionnels

* Programme de première NSI:  <https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf>
* Sujet 0 de l'épreuve de contrôle continue de fin de première :
	* <https://cache.media.eduscol.education.fr/file/Annales_zero_BAC_2021_1e/87/9/S0BAC21-1e-SPE-NSI_1133879.pdf>
	* Sujet 0 qui avait été mis en ligne puis remplacé <https://framagit.org/flaustriat/nsi-1/blob/master/docs/sujet00.pdf>
## Aide-mémoire (cheat sheet)

* Python : <https://perso.limsi.fr/pointal/_media/python:cours:mementopython3.pdf>
* HTML / CSS :   <http://cyrilaudras.fr/attachments/article/3/MementoHTML-CSS.pdf>
* Bash : <https://framagit.org/flaustriat/nsi-1/blob/master/menentoBash.pdf>

## Divers

* Aide-mémoireinformatique anglais-français : <https://www.gregoirenoyelle.com/wp-content/uploads/2010/03/Aide_memoire_informatique-Eng_fr.pdf>
* Caractères spéciaux et quelques racourcis : <https://framagit.org/flaustriat/nsi-1/blob/master/utilisationClavier.md>
